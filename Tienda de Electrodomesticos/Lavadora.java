public class Lavadora{
private float cargaMaxima;
private String funcionalidad;

public Lavadora(){
	cargaMaxima = 0;
    funcionalidad = "";
}	

public Lavadora(float cargaMaxima, String funcionalidad){
	this.cargaMaxima = cargaMaxima;
	this.funcionalidad = funcionalidad;	
}

public void showLavadora(){
	System.out.println("Carga maxima = " + cargaMaxima);
	System.out.println("Funcionalidad =  " + funcionalidad);
}	
	
}