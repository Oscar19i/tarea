public class productos{
	private String marca;
	private String modelo;
	private String estado;
	private int numSerie;
	private int voltaje;
	private float precio;
	

public productos(){
	marca = "";
	modelo = "";
	estado = "";
	numSerie = 0;
	voltaje = 0;
	precio = 0;
}	
	
public productos(String marca, String modelo, String estado, int numSerie, int voltaje, float precio){
	this.marca = marca;
	this.modelo = modelo;
	this.estado = estado;
	this.numSerie = numSerie;
	this.voltaje = voltaje;
	this.precio = precio;
}	

public void showProductos(){
	System.out.println("Marca: " + marca);
	System.out.println("Modelo: " + modelo);
	System.out.println("Estado: " + estado);
	System.out.println("Numero de serie: " + numSerie);
	System.out.println("Voltaje : " + voltaje);
	System.out.println("Precio = $" + precio);
}	
	
}