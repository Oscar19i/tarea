public class Heladera{
private float capacidad;
private String frost;

public Heladera(){
	capacidad = 0f;
	frost = "";	
}

public Heladera(float capacidad, String frost){
	this.capacidad = capacidad;
	this.frost = frost; 
}

public void showHeladera(){
	System.out.println("Capacidad en litros = " + capacidad);
	System.out.println("Frost = " + frost);
}

}