public class Licuadoras{
	private int potWatts;
	private int velocidades;
	
public Licuadoras(){
	potWatts = 0;
	velocidades = 0;
}	
	
public Licuadoras(int potWatts, int velocidades){
	this.potWatts = potWatts;
	this.velocidades = velocidades;
}	

public void showLicuadoras(){
	System.out.println("Potencia watts = " + potWatts);
	System.out.println("Velocidades = " + velocidades);
}

	
}